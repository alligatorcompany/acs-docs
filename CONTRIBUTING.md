# ACS dmstack Documentation Contributing

## About this project

The dmstack is a consulting product made by Alligator Company Software GmbH. It is helping our clients to start moving their data analytics or warehouse programs towards the modern practices of cloud with horizontal scalability, growing data teams and the need for speed - while still maintaining quality and longevity of their investments.

We achieve this by providing ready-to-use templates and guidelines for DataOps, Kubernetes GitOps, Analytics Engineering with DBT and git good practices for data teams. Accompanied with workshops, training and this documentation.

## Way of working

This documentation as part of our contsulting product is supposed to help data teams to implement modern data platforms and should therefore be generally Vendor neutral. But we currently suppot certain Datavault automation solutions which themselves only support certain database Vendors. Therefor minimal part is vendor specific, but it should stay that way.

Also, we cannot accept contributions, that are client specific or concentrate on specific parts of the industry. While we try to add more complicated samples that can demonstrate ease-of-use dispite being more complicated, it should always be adaptable and neutral for all industries.

Since we chose to publish under commons createve share-alike with attribution, you can always fork and start your own version.

## Principles

The current opinion about how to implement the data architecture, the pipelines and the usage of the tools is shared among the maintainers and will have to stand for consistency within the context of this consulting product. There are a lot of good and smart ideas how to go about this topic out there and the maintainers will always be willing to listen to those and might change their opinion.

We are trying to explain and state our view of the data management space, but are always open to and aware of the fact that there are always plenty of paths that lead to one solution. We are confident of our expertise in the field, but never preach and always accept and value criticism.

Solution oriented
Always provide a solution - even if you have a bad example, finish off the topic or paragraph with stating a solution to the problem.
Whenever possible, offer a solution
While it's important to be direct in error messages, it's equally important to be helpful. Avoid messaging that focuses on the problem without offering a solution or way forward.

- Avoiding Technical Details
- Always explain technical language, acronyms and TLAs (Three Letter Acronyms)
- Write in Plain English
- Value simple sentence structure over sophisticated language
