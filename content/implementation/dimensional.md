---
title: "Q2 Dimensional"
date: 2021-11-23T03:01:15+01:00
draft: false
---

## What is a dimensional model?

The dimensional model differentiates concepts of facts (measures) and dimension (context), where facts are typically numeric values that should be additive (aggregated) and dimensions are groups of hierarchies and descriptors to describe the facts. They depict the analytic paths the user can walk in order answer the questions he has about the specific business process and support the decision making process.

{{< figure src="/dimmodel_explain.png" title="Dimensional Model" alt="Dimensional Model Physical">}}

In contrast to Ralph Kimball's Top-Down Kimball - where the dimensional model actually implements the whole EDW - in our Datavault based EDW the dimensional model is just the default delivery model for our reporting applications.

Therefore, once we get to designing our data application models, the data integration and business driven modeling of hierarchies (ontologies) ideally have already been added to the Datavault model. This means the process for dimensional models should be much simpler than in a traditional Kimball warehouse, because there you would need to start talking to your business community to identify the business process and also consult the sources in order to find the correct grain of your fact tables and dimension hierarchies and attributes.

The dimensional model describes logical model parts (Fact, Dimension, Hierarchy), as seen above. The relational, physical representation of this dimensional model when implemented in a DBMS can be a Star Schema or a Snowflake Schema. We always default to Star Schema representations where the Hierarchies are de-normalized into single Dimension tables. This is especially helpful since we consider "virtual" view implementation as our model materializations in DBT.

Some important differentiators comparing Datavault EDW and the Dimensional application models: (To get your head around the differences between Datavault and Dimensional also considering the video linked below.)

{{< columns >}} <!-- begin columns block -->
## ELM
- __Model Goal__: Integration & Historization
- __Scope__: Enterprise Level
- __Relationships__: NBR
- Non-Event Based CBCs
- No Location or Date/Time Ensembles
- No dummy records - use multiple NBR instead
<---> <!-- magic separator, between columns -->

## Dimensional
- __Model Goal__: Reporting & Analysis
- __Scope__: Department / Function
- __Relationships__: Derived Associations, multiple grain and levels, based on in-scope metrics and facts - "Calcs we make"
- Singe Grain, Foundational Dimensions
- Use "Location & Date/Time/Calendar" Dimensions
- Use dummy records for optional parts of your fact grain
{{< /columns >}}

But the design principals, process and reasoning for a dimensional model to deliver our data applications have remained the same - regardless of the underlying data integration layer:

1. __Easy Navigation__ for Users - for power users that access the database directly or develop new applications based on existing parts.
2. __Query performance__ - still a performant physical model to implement in persistent tables, but might become less relevant if you think about virtualization. Since we implement the dimensional models with DBT, all parts will be materialized as "virtual" views by default.
3. __Easy Tool integration__ - most BI or Analytics tools still prefer or function pretty well using dimensional or star schema based relational constructs.

## Design Process

According to Ralph Kimball, the power of dimensional models come from careful adherence to ["the grain"](https://www.kimballgroup.com/2007/07/keep-to-the-grain-in-dimensional-modeling/) - which is the business definition of the measurement event that creates a fact record.

1. Select the Business Process: As described in the Data Story and ELM for the report application, also relevant subject areas and CBD have been defined already to choose from.
2. Declare the Grain: The grain establishes exactly what a single fact table row represents - e.g. Think about which imported foreign keys or dimension keys will make up the 
3. Identify the Dimensions: Dimensions provide the “who, what, where, when, why, and how” 
4. Identify the facts: Facts are the measurements that result from a business process event and are almost always numeric. A single fact table row has a one-to-one relationship to a measurement event as described by the fact table’s grain

## Relevant Dimensional Model Constructs

There are quite a few constructs that need to be considered to model all relevant situations that could arise, but luckily the dimensional model has been around since the 1990s and has actually not changed much in the years. For details please consider visiting links below.

{{< figure src="/dimmodel_constructs.png" title="Dimensional Constructs" alt="Dimensional Model Constructs">}}

In the context of our approach - as mentioned above most constructs mentioned in the above figure are valid - especially on the logical level - but from our experience the following good practices are worth mentioning:

- Do not Store NULL foreign keys in fact tables - use Dummy records instead. Any dimension might once be used in a scenario where it is optional concerning the fact table grain. Therefore all dimensions - especially 
- Dimension hierarchies and attributes should be declared and by the business or corresponding analytics engineers in order to provide common understanding and consistent usage of dimensional constructs. Especially common or reusable dimensions and facts should be well described and documented using the DBT documentation features.
- Year-to-Date and facts: Business users often request year-to-date (YTD) values in a fact table. It is hard to argue against a single request, but YTD requests can easily morph into “YTD at the close of the fiscal period” or “fiscal period to date.” A more reliable, extensible way to handle these assorted requests is to calculate the YTD metrics in the BI applications or OLAP cube rather than storing YTD facts in the fact table.
- Multiple Currency Facts: Good practice to store a common standard currency in the fact table and implement a second fact table for the conversion factors between currencies. Make sure the BI Layer efficiently caches or queries the two fact tables using one-pass SQL.
- Multiple Units of Measure Facts: Some business processes require facts to be stated simultaneously in several units of measure. For example, depending on the perspective of the business user, a supply chain may need to report the same facts as pallets, ship cases, retail cases, or individual scan units. If the fact table contains a large number of facts, each of which must be expressed in all units of measure, a convenient technique is to store the facts once in the table at an agreed standard unit of measure, but also simultaneously store conversion factors between the standard measure and all the others. The conversion factors must reside in the underlying fact table row to ensure the calculation is simple and correct, while minimizing query complexity.
- Late arriving Facts - best handled in virtual marts, since the granular Datavault EDW will usually handle that correctly. If virtual is not an option due to performance reasons, then in DBT using an incremental model with periodic full-refresh cycles could be used.
- Datavault Hash keys or Surrogate IDs can be used as surrogate keys to construct the Star Schema implementation. This especially makes view based implementations (virtualization) simpler, because there is now need to generate new surrogate keys to connect Fact and Dimension tables. Additionally it guarantees optimal join paths within the Datavault.
  - We usually recommend using binary datatype for hash keys - but some tools have been shown bad results using binary datatype for star schema implementation. If encountering those problems we advice to cast hash keys directly in your stage models `CAST(HK as CHAR(32))` in case of MD5 hashes.

### Fact Tables
#### Regular - Usually event based - also called transaction table with additive measure attributes and dimensional foreign keys

HK_CUSTOMER | HK_STORE | HK_SALES_MANAGER | HK_PRODUCT | QTY | AMT
------ | ------ | ------ | ------ | ------ | ------
0x0001 | 0x0002 | 0x0002 | 0x0002 | 2 | 9.99 

#### Snapshot
Inventory or account levels for a period of time - day, week or month

#### Accumulated
Accumulating snapshot fact table summarizes measure in predictable steps - a pipeline or workflow

#### Aggregate
Performance enhancing construct to pre-calculate measures that otherwise would take to long to be delivered.

#### Factless Fact
Combination of dimension keys occurrences in an event without any measures - like a student attending a class

#### Degenerate Dimension
Context information in fact tables besides measures. More often seen in modern column based DBMS to avoid join on fact grain dimension tables - this is a performance construct.

#### Additive, Semi-additive and Non-additive facts
Numeric measures in a fact table should be additive by default. Additive measures can be summed across all dimensions in the fact table. Accordingly semi-additive can only ba aggregated to some of the dimensions - whereas non-additive - such as ratios - cannot be aggregated at all. A good approach for non-additive facts is , where possible, to store the fully additive components into the final answer and use the BI-layer to calculate ratios.

### Dimension Tables
#### Regular
HK_PRODUCT | ID | Name | Size | Color
------ | ------ | ------ | ------ | ------
0x0002 | 4711 | Mountain Bike | 43 | Silver 

#### Junk
Flags and indicators for a process can be combined into a single junk dimension - as "transaction profile" where all possible or occurring combinations are stored.
#### Mini
The mini dimension is a performance construct, when attributes of low cardinality are separated from bigger dimensions.
#### Outrigger
Exceptional star schema construct where a dimension references a "secondary dimension" - but not for normalization (snowflake). Please use sparingly - consider correlation through fact table instead. 


## Further Information
- [Dimensional Modeling](https://en.wikipedia.org/wiki/Dimensional_modeling)
- [Kimball](https://www.kimballgroup.com/data-warehouse-business-intelligence-resources/kimball-techniques/dimensional-modeling-techniques/)
- [Dimensional Warehouse to Datavault](https://www.youtube.com/watch?v=--OJpdPeH80)
- [Facts and Dimensions](https://www.youtube.com/watch?v=6k3nwXXpnMY)
- [PowerBI & Star Schema](https://docs.microsoft.com/en-us/power-bi/guidance/star-schema)
- [Model Storming](https://modelstorming.com/)
- [Star Schema Handbook](https://books.google.pl/books/about/The_Star_Schema_Handbook.html?id=jENlT-_hP38C&redir_esc=y)
