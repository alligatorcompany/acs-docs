---
title: "HowTo"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# HowTo Articles

Within these HowTo articles it is shown how to execute development tasks in the data platform.

For all those HowTo we assume that the following components are properly configured and working prior to executing the HowTo Tasks.

1. SSH-key and agent running locally
2. Access to a Gitlab repository with deploy key and ssh key configured
3. kubernetes development cluster created with ACS workbench stagecreate 
4. DBT running locally
5. In some cases a sample model deployed on a different disposable environment to develop against

- Dataset A2Z
