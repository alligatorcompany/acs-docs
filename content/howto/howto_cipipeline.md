---
title: "HowTo CI pipeline"
date: 2021-11-23T03:01:15+01:00
draft: true
---

# HowTo CI pipeline

## Principles

- fast runtime
- runtime like in production
- testing - security & compliance tests scan, linting, unit testing
 
- running pipeline on all branch commits
- running pipeline on MR create

## Local machine preparation
- ssh connectivity
- git config user.name and user.email
- git config pull merge default
- 

## On-prem

## Gitlab integration

### Developer view

.gitlab-ci

gitlab runners

## Github integration
