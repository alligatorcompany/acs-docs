---
title: "02 Continuous Integration"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Continuous Integration

The continuous integration pipeline is based on our GIT repository that should be hosted on gitlab or github as central repository service.

According to DevOps experts (like [Martin Fowler and Dave Farley]({{< relref "#further-reading" >}}) ), we also suggest to always integrate into one common mainline or master branch. Further branching into releases, sprints or environments are usually too confusing and do not follow CI best practices. Minimizing the usage of branches and ensuring their short lifetime is also minimizing merge conflict resolutions, that is also saving developer time.
{{< figure src="/ci_overview.png" title="Overview CI and CD" alt="Continuous Integration & Deployment Schema" >}}

In the overview we can see the distinction between Continuous Integration/Delivery/Deployment. 
CI is before a version is created in the repository - as possible production ready increment. Continuous deployment only starts after the versioned increment has been created.

Generally we follow the rules of Martin Fowler for Continuous integration:

These are organizational habits that need be be learned by teams:
- Fix broken builds immediately: This is another 
- Everyone commits to the Mainline every day

Of course these are idealistic circumstances, that might not be reached, ever. But the team should thrive towards those as goal.

Followed rules: 
- Maintain a single repository: This is one reason we recommend to start with a Monorepo structure.
- Automate the build: This is realized though the CI argo workflow
- Make your build self-testing: Data seeds make sure we have no dependency on source systems and DBT test executes all tests automatically.
- Every commit should build the mainline on an integration machine: Since we are talking about DataOps, where the build does involve a quite complicated data pipeline in addition to our SQL code - we need to compromise a bit on this. We recommend to at least integrate the run into the merge/pull request logic - every merge request to start and then every merge to mainline.
- Keep the build fast: Again due to the involvement of a data pipeline we need to allow a little bit more time than on DevOps projects - maximum 20 minutes is a good starting point
- Make it easy for Anyone to get the lates executable: This is the reason why we have sprint based branches - which live until they are deployed to the production environment. Teams can easily start disposable envs based on those branches in order to compare and test. 
- Everyone can see what is happening: There should be no lock-in of any environment or GIT branch of single users or groups - the shared code ownership takes precedence because we need to take over tasks anytime.
- Automate deployment - this is CD not CI - although most parts of the deployment are already able to be automated - sqitch, datavault builder and DBT support this immanently
- Test in a clone of the production environment - this currently means we cannot have this test environment as disposable, because datavault builder and Exasol do not support Kubernetes production deployments. All other components could be productive in a Kubernetes cluster.

## Isolated DEV environments
The isolated DEV environments are very important for the overall development performance and crucial in order to achieve short cycle times for our user's feature requests.
They are the way to support developers to work independently and on smaller increments. The disposable environments create all necessary components in a Kubernetes namespace on the development cluster. It is an empty starting point where the developer now has the freedom to decide - based on the feature/bugfix/refactor ticket he is working on - what to deploy. He needs to only pick the relevant artifacts in all parts of the data architecture:
- Hardrules: which sources do I need to involve?
- Datavault: which CBCs are involved?
- Softrules: which application projects are involved?

After finishing the changes the developer can commit the artifacts that have changed in the GIT repository - independently of the DEV environment being worked on. The only truth of the DWH artifacts is in the GIT repository.

## Branching
In order to coordinate the concurrent development of multiple features and sprint, we do recommend using sprint branches - per team. That way we can use those results to base and compare with current feature developments. Especially for incremental developments with automation software it is really handy to only use parts of the complete model to focus your develop work on those artifacts that are currently needed. 
Based on these sprint branches, 

In order to facilitate merge or pull requests in the respective git hosting solution, we also encourage the usage of feature branches. But they should only live as long as the story and should always be deleted with the merge back into the mainline. We suggest to prefix branch names with functions (function/ticket_number-description) as follows:
- bugfix/<<ticket_number>>-sales-replace-KPI-calculation for all bugfix branches
- feature/<<ticket_number>>-sales-order-CBC for all features
- refactor/<<ticket_number>>-merge-satellites-CBC in case of refactoring 
- release/<<sprint_name>> for the sprints / releases

![trunk based development](https://trunkbaseddevelopment.com/trunk1c.png)


This way we have simple transparency over where to look on further details in the ticketing system and our default merge commit messages will also contain this information by default.

## CI Pipeline execution
Our CI pipeline is based on the ACS-workbench and Argo workflows for the execution of the different steps along the data architecture. This way we are almost independent of the underlying Gitlab CI execution. We only need a OCI compliant container environment for the workbench and a kubernetes cluster to run against. Here we rely on the environment settings and correct kubeconfig prior to execution.

Additionally we need to get the git commit hash to relate to the point in the git history and the project directory root directory in order to execute the following steps within the context of the GIT provided containerd environment:

1. envcreate - Namespace with datavault automation and configured access to target DWH database. The namespace should contain the commit hash - by default we call this test-CI_COMMIT_SHORT_SHA
2. kubectl apply -f CI_PROJECT_DIRwf/template - to install argo workflow templates
3. argo submit --wait --log -n test-CI_COMMIT_SHORT_SHA wf/ci.yaml - execution of the ci Argo workflow

Here is an example .gitlab-ci.yml:
```
stages:
  - build
  - test
  - publish

default:
  image: alligatorcompany/acs-workbench:1.0.13
  timeout: 90m

workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

test-job:
  stage: build
  script:
  - PATH=$PATH:/home/acs/bin /home/acs/bin/envcreate test-$CI_COMMIT_SHORT_SHA
  - PATH=$PATH:/home/acs/bin /home/acs/bin/kubectl -n test-$CI_COMMIT_SHORT_SHA apply -f $CI_PROJECT_DIR/wf/template
  - PATH=$PATH:/home/acs/bin /home/acs/bin/argo submit --wait --log -n test-$CI_COMMIT_SHORT_SHA $CI_PROJECT_DIR/wf/ci.yaml
```

All concrete CI pipeline steps are now being executed in the configured kubernetes cluster via Argo - usually this is the dev-cluster. This now looks much like a "normal" DWH run in production - except we are replacing the SOR - Schema on Read - Views with the seed files we created as replacement for our source interfaces.

A Deployment:
1. Executing acs-deploy sqitch for initial DWH structures
2. Executing Datavault Builder deployment to create initial schemas for automation software

B Dataflow execution:
1. Executing Sourcing - dbt seed on 00_src/psa
2. Executing Hardrules - dbt run on all 00_src/ - Hardrule projects 
3. Testing Hardrules - dbt test on all 00_src/ - Hardrule projects 
4. Executing Datavault - Automation - source based
5. Executing Softrules - 02_app/ all volatile softrules
6. Executing Datavault - Automation rules based
7. Executing Softrules - dbt run 02_app/ all non-volatile softrules
8. Testing Softrules - dbt test on all 02_app/ - Softrule projects

And here is the example CI argo workflow:
```
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: fifa-ci-
spec:
  entrypoint: startdag
  arguments:
    parameters:
      - name: revision
        value: main

  templates:
  - name: startdag
    dag:
      tasks:
      - name: deploy
        templateRef: 
          name: wf-dvb-deploy
          template: wf-dvb-deploy
      - name: seed
        templateRef: 
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'seed'}, {name: projectdir, value: '/code/00_src/psa/'}]
      - name: hardrules
        dependencies: [ seed ]
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'run'}, {name: projectdir, value: '/code/00_src/fifa_hr/'}, {name: target, value: 'ci'}]
      - name: hardrules-test
        dependencies: [ hardrules ]
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'test'}, {name: projectdir, value: '/code/00_src/fifa_hr/'}, {name: target, value: 'ci'}]
      - name: datavault
        dependencies: [ hardrules, deploy ]
        templateRef:
          name: wf-dvb-run
          template: wf-dvb-run
        arguments:
          parameters: [{name: JOBS, value: 'JOBS=/code/01_dv/dvb/RAW_jobs.csv'}]
      - name: softrules
        dependencies: [ datavault ]
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'run'}, {name: projectdir, value: '/code/02_app/fifa_report/'}, {name: target, value: 'ci'}]
      - name: bdv
        dependencies: [ softrules ]
        templateRef:
          name: wf-dvb-run
          template: wf-dvb-run
        arguments:
          parameters: [{name: JOBS, value: 'JOBS=/code/01_dv/dvb/BDV_jobs.csv'}]
      - name: apps
        dependencies: [ bdv ]
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'run'}, {name: projectdir, value: '/code/02_app/fifa_report/'}, {name: target, value: 'ci'}]
      - name: apps-test
        dependencies: [ apps ]
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'test'}, {name: projectdir, value: '/code/02_app/fifa_report/'}, {name: target, value: 'ci'}]
```

# Further Reading
- [Martin Fowler](https://martinfowler.com/articles/continuousIntegration.html)
- [Dave Farley](https://www.youtube.com/channel/UCCfqyGl3nq_V0bo64CjZh8g)
- [Trunk Based Development](https://trunkbaseddevelopment.com/)
- [Git](https://www.atlassian.com/git/tutorials)
- [DevOps vs. DataOps](https://devops.com/dataops-vs-devops-whats-the-difference/)
- [CI and CD pipeline](https://www.redhat.com/en/topics/devops/what-cicd-pipeline)
- [CI wikipedia](https://en.wikipedia.org/wiki/Continuous_integration)
