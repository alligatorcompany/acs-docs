---
title: "03 DataOps"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# DataOps

The operational aspects of our DataOps platform are also oriented along our data architecture and the tools that need to be executed in a production to run of our data warehouse.

{{< figure src="/ops_dag_overview.png" title="Basic DAG to load data architecture" alt="DataOps DAG Overview">}}

The basic Directed Acyclic Graph (DAG), that represents our overall dataflow is 
1. Q3 Ingest (without dependencies) - [provided docker image](https://gitlab.com/alligatorcompany/acs-operation) with ingestion process 
2. Q3 PSA (depends on Ingest)- dbt project with incremental psa_insert model
3. Q3 Hardrules (without dependencies) - one DBT project per source with several interfaces and rule models for staging in the Datavault. Per definition no persisting models allowed.
4. Q1 Datavault - Automation workflow - provided docker image - including entity views
5. Q2 Softrules - volatile - one DBT project per application - with filter on models to be run
6. Q1/2 Datavault Business Vault persisted Softrules - Automation workflow 
7. Q2 Application - mixed virtual and persistent - one DBT project per application
8. Q4 Sandbox - data inputs if any per

{{< hint info >}}
**DBT virutal models could be considered for deployment of releases only,**  
but for the sake of easier dependency management and simpler DAG, we suggest to always run all DBT projects in your load cycles regardless whether the model version changed or not. The only exception being the Softrules that are considered to be persisted in the Business Datavault.
{{< /hint >}}


In order to execute the data pipeline during production load and during continuous integration, we have physical deployments and workflows to be executed in our environments.
We only have these kind of different steps that form the complete workflow:
1. Ingest for sourcing
2. DBT run
3. Datavault run

For release deployment of additional artifacts, we have more steps to consider - which are being executed once for release deployment
1. Sqitch deployment (once per release only)
2. Datavault deployment - model changes and if necessary model migrations
3. Sandbox deployment - providing data inputs for our data experiments

## Argo Workflows

### metrics https://argoproj.github.io/argo-workflows/metrics/

Default Controller Metrics¶
Metrics for the Four Golden Signals are:

Latency: argo_workflows_queue_latency
Traffic: argo_workflows_count and argo_workflows_queue_depth_count
Errors: argo_workflows_count and argo_workflows_error_count
Saturation: argo_workflows_workers_busy and argo_workflows_workflow_condition

Metrics are defined in-place on the Workflow/Step/Task where they are emitted from. Metrics are always processed after the Workflow/Step/Task completes, with the exception of real-time metrics.

```
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: model-training-
spec:
  entrypoint: steps
  metrics:
    prometheus:
      - name: exec_duration_gauge         # Metric name (will be prepended with "argo_workflows_")
        labels:                           # Labels are optional. Avoid cardinality explosion.
          - key: workflow_name
            value: "{workflow.name}"
        help: "Duration gauge by name"    # A help doc describing your metric. This is required.
        gauge:                            # The metric type. Available are "gauge", "histogram", and "counter".
          value: "{{workflow.duration}}"
```


Metric definitions must include a name and a help doc string. They can also include any number of labels (when defining labels avoid cardinality explosion). Metrics with the same name must always use the same exact help string, having different metrics with the same name, but with a different help string will cause an error (this is a Prometheus requirement).

All metrics can also be conditionally emitted by defining a when clause. This when clause works the same as elsewhere in a workflow.

A metric must also have a type, it can be one of gauge, histogram, and counter (see below). Within the metric type a value must be specified. This value can be either a literal value of be an Argo variable.

### 
Argo workflows are Kubernetes native way of executing Jobs. It runs the main container as regular pod with sidecars for watching and initializing, introducing custom resource definitions (CRD) for workflow specific semantics in YAML manifests. Those CRD allow you to define workflows as YAML definitions like any other resource using standard Kubernetes tooling. In our standard Monorepository the implementation is in the Repository in wf/... as different kind of YAML manifest:
- Workflow: one-off workflows to be executed - like the continuous pipeline ci.yml
- CronWorkflow: scheduled triggers in cron-like notation
- WorkflowTemplate: re-usable templates

```
├ dwh
...
└── wf
    └── template
        └── wf-daily.yml

```

The daily load is implemented as a template, that depicts above basic DAG. This template can be re-used for different schedules and source lists. As the source based delivery is the starting point for our deliveries.

For default implementation of daily load, we can parameterize the following:
- source names to be included in digest
- version tag / release branch name


Time based schedule for example is shown as follows:
```
➜ argo -n argo cron list
NAME                      AGE   LAST RUN   NEXT RUN   SCHEDULE     SUSPENDED
daily-asiapacific-ld9fx   70d   17h        6h         0 20 * * *   false
daily-europe-7d8k9        29d   6h         17h        0 7 * * *    false
daily-america-psxvl       70d   2h         21h        0 11 * * *   false 
```

This daily template contains all above defined steps of the dataflow along the data architecture. Each instance for a different timezone (here America, Europe and Asia-Pacific) will have it's own implementation of the template with different time and source lists for the different source systems.

```
apiVersion: argoproj.io/v1alpha1
kind: CronWorkflow
metadata:
  generateName: daily-europe-
spec:
  schedule: "0 1 * * *"
  concurrencyPolicy: "Replace"
  startingDeadlineSeconds: 0
  workflowSpec:
    entrypoint: daily

    templates:
    - name: daily
      dag:
        tasks:

        - name: daily
          templateRef: 
            name: wf-daily
            template: daily
          arguments:
            parameters:
              - name: sourcelist
                value: "fifa"
```

Schedule times in Argo by default are UTC - but that could also be re-configured.

Further schedules work similarly - just with different triggers - please refer to the Argo-Events() for kind of triggers supported.

List of active or recent workflows can be listed as follows:
```
➜ argo -n argo list
NAME                                  STATUS                AGE   DURATION   PRIORITY
daily-america-psxvl-1642158000        Succeeded             2h    1h         0
daily-europe-7d8k9-1642143600         Succeeded             6h    3h         0
daily-asiapacific-ld9fx-1642104000    Succeeded             17h   1h         0
daily-america-psxvl-1642071600        Succeeded             1d    1h         0
daily-europe-7d8k9-1642057200         Succeeded             1d    3h         0
daily-asiapacific-ld9fx-1642017600    Succeeded             1d    1h         0
daily-america-psxvl-1641985200        Succeeded             2d    1h         0
daily-europe-7d8k9-1641970800         Succeeded             2d    3h         0
daily-asiapacific-ld9fx-1641931200    Succeeded             2d    1h         0
```

Run results can be seen with the argo watch/get command:
```
➜ argo -n argo get daily-america-psxvl-1642158000
Name:                daily-america-psxvl-1642158000
Namespace:           argo
ServiceAccount:      default
Status:              Succeeded
Conditions:          
 PodRunning          False
 Completed           True
Created:             Fri Jan 14 12:00:00 +0100 (2 hours ago)
Started:             Fri Jan 14 12:00:00 +0100 (2 hours ago)
Finished:            Fri Jan 14 13:22:52 +0100 (1 hour ago)
Duration:            1 hour 22 minutes
Progress:            17/17
ResourcesDuration:   3h20m27s*(1 cpu),3h20m27s*(100Mi memory)

STEP                               TEMPLATE               PODNAME                                    DURATION  MESSAGE
 ✔ daily-america-psxvl-1642158000  daily                                                                         
 └─✔ daily                         wf-daily/daily                                                                
   ├─✔ ingest                      wf-ingest/wf-ingest    daily-america-psxvl-1642158000-2430810240  4m          
   ├─✔ fifa                        wf-elt-run/wf-elt-run  daily-america-psxvl-1642158000-154822349   1m          
   ├─✔ psa                         wf-elt-run/wf-elt-run  daily-america-psxvl-1642158000-229275216   40m         
   ├─✔ datavault(0)                wf-dvb-run/wf-dvb-run  daily-america-psxvl-1642158000-529781903   33m         
   ├─✔ common                      wf-elt-run/wf-elt-run  daily-america-psxvl-1642158000-2145768766  1m          
   ├─✔ sbx_sop                     wf-elt-run/wf-elt-run  daily-america-psxvl-1642158000-655517578   1m          
   └─✔ app_fifa                    wf-elt-run/wf-elt-run  daily-america-psxvl-1642158000-1710888100  2m          
```

Detailed runtime information can be retrieved via kubectl log command:
```
➜ kubectl logs -n argo -c main daily-america-psxvl-1642158000-229275216
Running with dbt=0.21.0
No profile "dbt" found, continuing with no target
Installing dbt-labs/dbt_utils@0.7.1
  Installed from version 0.7.1
  Updated version available: 0.8.0

Updates available for packages: ['dbt-labs/dbt_utils']                 
Update your versions in packages.yml, then run dbt deps
Running with dbt=0.21.0
Found 1 model, 0 tests, 0 snapshots, 0 analyses, 360 macros, 0 operations, 151 seed files, 1 source, 0 exposures

11:04:51 | Concurrency: 5 threads (target='dev')
11:04:51 | 
11:04:51 | 1 of 1 START incremental model YYY_Q3_PSA.psa_insert................. [RUN]
11:44:52 | 1 of 1 OK created incremental model YYY_Q3_PSA.psa_insert............ [OK in 240.53s]
11:44:52 | 
11:44:52 | Finished running 1 incremental model in 2401.28s.

Completed successfully

Done. PASS=1 WARN=0 ERROR=0 SKIP=0 TOTAL=1
```

## Loading the Data Architecture

### Physical Schema in Database
In order to have alphabetical ordering in the physical view into our database, by default the following naming scheme is used:

- ZZZ_Q3_<SOURCE> for all source DBT projects
- YYY_Q3_PSA for the PSA DBT project
- XXX_Q1_<SUBJECT_AREA> for Datavault if the automation allows for custom schema names
- AAA_Q2_APP_<APP_NAME> for DBT projects with application names as suffixes
- AAA_Q4_SBX_<SBX_NAME> for Sandboxes

{{< figure src="/da_impl_detail.png" title="Physical database schemas" alt="Database Schemas" >}}

This way you always get the outgoing schemas first in the list and the quadrants are also transparent to everyone looking in the database.

### Database Schema deployment - sqitch

Sqitch is used to initially "deploy" DDL into the DWH Database, if anything is needed before you start your first DBT model (PSA in our case). 

Sqitch is a database change management application - like other schema versioning utilities. Sqitch supports PostgreSQL, Exasol and Snowflake and describes itself as
- No Opinions: standalone - no part of other, larger framework
- Native scripting - you write native SQL to your engine
- Dependency resolution - declare dependencies instead of versioned order
- Deployment integrity
- Iterative development

Sqitch artifacts are stored in default repository location dwh/deploy, dwh/verify and dwh/revert.

Using the acs-deploy docker image, sqitch can be used to install structures and/or DDL into the Data Warehouse that is not under the control of DBT or the Datavault automation. This typically includes
- PSA structures
- Connection definitions source systems for import
- Grant setups for security guidelines
- Datavault Builder structures

### Load types
#### Ingestion
The provided container image contains the necessary libraries and calls the `ingest/ingest.py` routines for
- Relational databases using sqlalchemy
- DWH vendor specific import CLI
- s5cmd for S3 uploads into cloud storage

This starting python script is being executed and parameterized with the following positional parameters:
1. project: directory of the root GIT repository
2. sources: comma separated list of sources to include in the ingest run

Additional parameters are included using environment / secrets as for all components in the argo workflow. Here a sample YAML definition for the ingest:
```
apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: wf-ingest
spec:
  templates:
  - name: wf-ingest
    inputs:
      parameters:
        - name: sources
...
    container:
      image: alligatorcompany/acs-ingest:1.0.8
      command: ["/bin/bash"]
      args: ["-c", "source /venv/*/bin/activate && python -u /code/ingest/ingest.py /code/ {{inputs.parameters.sources}}"]
      volumeMounts:
        - name: workdir
          mountPath: /code
      envFrom:
          - secretRef:
              name: dwhsrc-credentials
      env:
      - name: EXASOL_DSN
        valueFrom:
          secretKeyRef:
            name: dataloader-credentials
            key: exasol-url
      - name: EXASOL_USER
        valueFrom:
          secretKeyRef:
            name: dataloader-credentials
            key: username
      - name: EXASOL_PASS
        valueFrom:
          secretKeyRef:
            name: dataloader-credentials
            key: password
      - name: EXASOL_SCHEMA
        value: 'YYY_Q3_PSA'
    volumes:
      - name: workdir
        emptyDir: {}
      - name: deploy-volume
        secret:
          secretName: gitlab-deploykey
          defaultMode: 0600
```

#### DBT execution
For all PSA, Hardrule, Softrule and other DBT projects that may be in the quadrant, the same DBt execution is being used. It is based on a container image called acs-elt-run.

The parameters are 
- command: DBT CLI command to be executed (run, test, seed, deps)
- vars: Variables that might be needed within your DBT models
- target: target profile - switching for CI to load based on seeds
- projectdir: the GIT repository based root of the DBT project to be executed

Here is the Argo template definition for DBT executions:
```
apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: wf-elt-run
spec:
  templates:
  - name: wf-elt-run
    inputs:
      parameters:
        - name: command
        - name: args
          value: ''
        - name: vars
          value: ''
        - name: target
          value: 'ci'
        - name: projectdir
          value: '.'
...
    container:
      image: alligatorcompany/acs-elt-service:1.0.2
      command: ["/bin/bash"]
      args: ["-c", "dbt deps --project-dir={{inputs.parameters.projectdir}} && dbt {{inputs.parameters.command}} {{inputs.parameters.args}} {{inputs.parameters.vars}} --project-dir={{inputs.parameters.projectdir}} --profiles-dir={{inputs.parameters.projectdir}}"]
      volumeMounts:
        - name: workdir
          mountPath: /code
      env:
      - name: EXASOL_SERVICE_HOST
        valueFrom:
          secretKeyRef:
            name: dataloader-credentials
            key: exasol-host
      - name: EXASOL_SERVICE_PORT
        valueFrom:
          secretKeyRef:
            name: dataloader-credentials
            key: exasol-port
      - name: DBT_USER
        valueFrom:
          secretKeyRef:
            name: dataloader-credentials
            key: username
      - name: DBT_PASS
        valueFrom:
          secretKeyRef:
            name: dataloader-credentials
            key: password
...
```

The Argo template for dbt runs is used in this step. The step looks like this:
```
apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: wf-daily
spec:
  entrypoint: daily

  templates:
  - name: daily
    inputs:
      parameters:
        - name: sourcelist
    dag:
      tasks:
...
      - name: psa
        dependencies: [ ingest ]
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'run'}, {name: projectdir, value: '/code/00_src/psa/'}]

```

- PSA

The PSA project is by default located in to 00_src/psa folder(https://gitlab.com/alligatorcompany/fifadwh/-/blob/master/00_src/psa/models/psa_insert.sql) with default psa_insert incremental model. As mentioned in the data architecture, this is a generalized table structure which saves all records for all sources as JSON payload.
The PSA only stores delta increments on every run - according the the technical key defined in the source.yml of the Hardrule project. If an interface has no technical key, the whole record is taken as key. That is one reason to have the BK per record stored as MD5, to have easier access path for the DBMS. 


- Hardrules and Softrules



#### Datavault
```
├ dwh
...
└── 01_dv
    └── dvb
    |   └── datavault_builderobjects / ...
    |   └── version.json
    |   └── rollout.sh
    |   └── RAW.csv
    |   └── BDV.sh
    └── dbt --> lineage project for dbt when using Datavault Builder
...
```
In case of the DBTvault automation, the standard DBT execution will be used.

For the Datavault Builder automation software we provide a ready to use docker image - acs-dvbrun.
The only parameter is a CSV file, that contains a list of Datavault Builder job names to be executed.
In order to differentiate between Hardrules loaded and Softrules loaded jobs by default there are the following two CSV files in the GIt repository:
- raw.csv: list of job names from Datavault Builder
- bdv.csv: list of job names from Datavault Builder to be executed after raw source loaded target model elements. These are the jobs that re-source the Softrules that should be persisted in the Datavault model. see also (/implementation/datavault/#business-vault)

Execution in Argo of the acs-dvbrun image in the following template:
``` 
apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: wf-dvb-run
spec:
  templates:
  - name: wf-dvb-run
    retryStrategy:
      limit: "3"
      retryPolicy: "Always"
    inputs:
      parameters:
        - name: JOBS
          value: 'JOBS=null.csv'
...
    container:
      image: alligatorcompany/acs-dvbrun:1.0.2
      command: [ "/bin/bash" ]
      args: [ "-c", "/work/job_exec.sh {{inputs.parameters.JOBS}}" ]
...
```


## Deployment
---
{{< figure src="/wb_overview.png" title="Infrastructure" alt="Deployment Environments" >}}

For the infrastructure part, we need to provide runtime environments for above workflows to execute on Kubernetes clusters.

In development, for disposable envs per developer and in Staging, for CI pipeline or other test environments, Kubernetes namespaces will guarantee necessary isolation of the different workloads. Although there could also be separate clusters for development and staging - this in the end depends on the overall infrastructure regarding git hosting, naming and knowledge regarding reverse proxies - last but not least its a matter of taste, really.

Production workloads will as of today not be completely deployed to Kubernetes, because not all Vendors support productive runtime on Kubernetes (yet). 
- Exasol offers a containerized version of its database, but support for productive workloads still depends on their own cluster OS
- Snowflake runs as SaaS in the cloud
- Postgres support on Kubernetes is not available
- Datavault Builder delivers and supports docker-compose containerized application

Therefor as depicted above, UAT / Pre-prod and Production stages will have those parts deployed on virtual machines and not in the Kubernetes version. It is always a good idea to have production servers duplicated for UAT tests and also for failover scenarios. For each additional stage a k3d/k3s cluster will be added to host Argo and monitoring functionality running in Kubernetes - as described below.

### Deployment of CI pipeline
In order for the continuous integration to execute, we need a deploy key or authorization token - depending on the git hosting service you are using.
As you can see in above secrets list - this deploykey also needs to be deployed in the default namespace. Depending on your system, the corresponding workflow templates need to be adjusted.

### Deployment of a Release

As in trunk based development, if you have no Continuous Deployment pipeline in place, release branches can be utilized in order to represent a stable place to operate the supported release on.

In our picture, this is the sprint release, that is representing the potentially, shippable product increment. One could think of sprint releases as minor increments - but usually for a company's analytics program releases, if executed in sprint rhythm, are not matter of semantic versioning. Bugfixes on those release branches are merged or cherry-picked from the trunk.

{{< hint warning >}}
**No development commits on release branches**  
Release branches are only for releasing and bug fixing, if necessary during sprint increments. They should also be deleted once follow-up sprints are productive.
{{< /hint >}}

Release branches can be created at the beginning of a sprint - to support a snapshot of current sprint development - but usually are created before actual deployment / releasing of the sprint content.

Once created, the following steps usually need to be taken into account:
1. Upgrading Q1 Datavault model
- Using Datavault Builder: From the 01_dv/dvb folder execute a [deployment on the target environment](https://datavault-builder.com/wp-content/uploads/documentation-portal/latest_6/_modules/deployment.html)
{{< hint info >}}
**Datavault Builder Release 6 - Automatic Deployment**
As of Release 6, Datavault Builder supports automatic deployment. But it is still recommended to execute this step prior to release, in order to make sure no data migrations are needed for refactoring the Datavault model. Only then one should create the corresponding package for automatic deployment.
{{< /hint >}}


2. Upgrading Q2 persisted models needed?
If you have any persisted models in the Q2 layer, changes need to be checked, whether one-time data migrations are needed - e.g. re-create incremental model.
These executions should be added to one-time workflow executions on the release branch for one-time execution in target environments.

3. Setting Q2 template for daily workflow
Within the daily template, select Q2 products and Q4 sandboxes, that might need to be added or removed.

4. Change release branch for workflow templates
In release branch based deployment, the templates on the release branch need to be configured to point the the correct git branch.
In case you are using docker images for deployment, the tags should be configured for workflow usage.

5. Finally, you can tag your release and kick-off deployment into target environment

### Deployment of documentation website

The documentation website for your Monorepo consists of three parts:

- Hugo website containing project specific documentation for infrastructure, deployment and agreed practices for SQL and naming standards
- Hugo website containing ACS Analytics Engineering DataOps guideline documentation containing 
- Aggregated catalog project from Hardrule and Softrule projects
- Alternatively: Datavault Builder documentation could be linked into production deployment

In order to combine these parts into one single page, deployable with nginx, the Dockerfile with multi-stage build can be used to combine those parts. Using the Hugo, DBT and nginx base images.

It is recommended to add this build and deploy process to your continuous integration and deployment pipelines.

## Stages - Cluster Creation
To create a stage based on kubernetes cluster, we are using k3d and k3s for lightweight installation process. Also using k3d, we only need a current dockerhost deployment to start our stagecreate script.

{{< figure src="/ops_k3d_k3s_overview.png" title="Sample k3s cluster created by k3d" alt="K3D, K3S, Dockerhost overview" >}}

The following env needs to be sourced before creating a new stage or executing other k3d commands:
```
DOCKERHOST=172.105.85.155
DOCKERIP=$DOCKERHOST
STAGE=dev
KUSTOMIZE_ENV=dev
DOCKERUSER=root
DOCKERURL=$(echo "ssh://$DOCKERUSER@$DOCKERHOST")
DOCKER_HOST=$DOCKERURL
APIPORT=1666
HTTPSPORT=443
SSHPORT=2222
ADDDNS=$(echo "8.8.8.8 8.8.4.4")
DOMAINNAME=$(echo "$DOCKERIP.nip.io")
DBT_USER=sys
DBT_PASS=<password>
DBT_PROFILES_DIR=.
EXASOL_SERVICE_HOST=localhost
EXASOL_SERVICE_PORT=8563
```
If not connected to the docker host via ssh, authentication with the docker host for the ssh connection needs to be configured - including ssh-agent. That is because k3d will execute a lot of commands over the ssh connection to the dockerhost. Hence a `ssh $DOCKER_HOST` should work without passwordless and the `docker context ls` command should refer to the local socket if dialed in to the dockerhost or the above set DOCKER_HOST environment variable usually through ssh.

The ```stagecreate``` command creates a new stage, that is a k3s kubernetes cluster as container images. After successful execution (return code 0), the following should be listed by ```k3d cluster list```:
```bash
➜ k3d cluster list
NAME   SERVERS   AGENTS   LOADBALANCER
dev    1/1       3/3      true
```

```bash
➜ kubectl get ns                  
NAME              STATUS   AGE
kube-system       Active   81d
default           Active   81d
kube-public       Active   81d
kube-node-lease   Active   81d
argo              Active   81d
argo-events       Active   81d
ingress-nginx     Active   81d
monitoring        Active   81d
```

{{< hint danger >}}
**Danger - this could delete everything**  
Executing ```stagedelete``` will remove the cluster completely by calling `k3d cluster delete $STAGE`. All disposable envs and data that might not be versioned in git yet, will be lost.
{{< /hint >}}


### Cluster Configuration

Additional configuration for the cluster are [secrets](https://kubernetes.io/docs/tasks/configmap-secret/) for disposable environments and git CI pipeline.

The secrets for the disposable environments are stored in the default namespace. These are:
- regcred - dockerjson format for [private registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) logins like for Datavault Builder
- dataloader-credentials - database access user and url for different databases
- dvb-secrets - license and internal security accounts
- db-secrets - dwh user for deployment
- gitlab-deploykey - repository access for automation like CI pipeline

```
➜ kubectl get secret
NAME                     TYPE                                  DATA   AGE
regcred                  kubernetes.io/dockerconfigjson        1      120d
dataloader-credentials   Opaque                                9      3d18h
gitlab-deploykey         Opaque                                3      3d18h
dvb-secrets              Opaque                                7      3d18h
db-secrets               Opaque                                2      41h
```

Gitlab specific CI pipeline configuration adds gitlab runner and tokens, that are described in Deployment of CI pipeline.

This is a sample script to create above secrets in the default namespace from local files:
(beware that there should not be a newline in these files - make sure your editor does not include a new line at the end automatically.)
```
#!/usr/bin/env sh
NS=default
if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'dataloader-credentials')" ]; then
    kubectl delete secret dataloader-credentials --namespace=$NS
fi
kubectl create secret generic dataloader-credentials \
    --namespace=$NS \
    --from-literal=encpass=$(cat dvbpass | base64) \
    --from-file=dbuser=dbuser \
    --from-file=dbpass=dbpass \
    --from-file=dvbuser=dvbuser \
    --from-file=dvbpass=dvbpass \
    --from-file=exasol-url=exasol_url \
    --from-file=dvb-url=dvb_url \
    --from-file=exasol-host=exasol_host \
    --from-file=exasol-port=exasol_port

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'gitlab-deploykey')" ]; then
    kubectl delete secret gitlab-deploykey --namespace=$NS
fi
kubectl create secret generic gitlab-deploykey --namespace=$NS --from-file=id_rsa=id_rsa --from-file=id_rsa.pub=id_rsa.pub --from-file=known_hosts=known_hosts

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'dvb-secrets')" ]; then
    kubectl delete secret dvb-secrets --namespace=$NS
fi
kubectl create secret generic dvb-secrets \
    --namespace=$NS \
    --from-file=systems_password_private_key_password=systems_password_private_key_password \
    --from-file=systems_password_public_key=systems_password_public_key \
    --from-file=authenticator_password=authenticator_password \
    --from-file=core_dbadmin_password=core_dbadmin_password \
    --from-file=datavault_builder_license=datavault_builder_license \
    --from-file=scheduler_password=scheduler_password \
    --from-file=systems_password_private_key=systems_password_private_key

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'db-secrets')" ]; then
    kubectl delete secret db-secrets --namespace=$NS
fi
kubectl create secret generic db-secrets \
	--namespace=$NS \
    --from-file=dbuser=dbuser \
	--from-file=dbpass=dbpass

kubectl apply -f regcred.yaml
```
## Monitoring & Alerting

### prometheus

### storage

### sql exporter push-gateway 

For monitoring we use tobs - to deploy a ready stack that adds timescaledb - for long-term storage of your Kubernetes events.
{{< figure src="/tobs.png" alt="Observability Stack Overview">}}

Under the hood, as depicted in above diagram, the well-known standard components from Kubernetes are used.

Argo workflows are exposing events, that can be visualized using a standard dashboard:

{{< figure src="/ops_argo_grafana.png" alt="Grafana">}}
{{< figure src="/ops_argo_metrics.png" alt="Argo Metrics">}}

Based on this metrics, alerts are configured using the alertmanager:
- Failure of workflow
- Average runtime of workflow deviates more than x%

### Observability installation
#### Installing the CLI tool

To download and install tobs, run the following in your terminal, then follow the on-screen instructions.

```bash
curl --proto '=https' --tlsv1.2 -sSLf  https://tsdb.co/install-tobs-sh |sh
```

Alternatively, you can download the CLI directly via [our releases page](https://github.com/timescale/tobs/releases/latest)

Getting started with the CLI tool is a two-step process: First you install the CLI tool locally, then you use the CLI tool to install the tobs stack into your Kubernetes cluster.

#### Using the tobs CLI tool to deploy the stack into your Kubernetes cluster

After setting up tobs run the following to install the tobs helm charts into your Kubernetes cluster

```bash
tobs install
```

This will deploy all of the tobs components into your cluster and provide instructions as to next steps.
# Further Reading
- [DataOps vs. DevOps](https://devops.com/dataops-vs-devops-whats-the-difference/)
- [Argo Workflows](https://argoproj.github.io/workflows)
- [tobs](https://www.timescale.com/blog/introducing-tobs-deploy-a-full-observability-suite-for-kubernetes-in-two-minutes/)
- [Bret Fisher Dev Ops](https://www.youtube.com/c/BretFisherDockerandDevOps)
- [DevOps Toolkit](https://www.youtube.com/c/DevOpsToolkit)
- [Datavault Loading Standards](https://tdan.com/data-vault-series-5-loading-practices/5285)
