---
title: "04 Analytics Engineering"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Analytics Engineering
Modern data platforms and data teams are all about data literacy and liberation - making data available to everyone in the organization. But those are mostly buzz words, that are rarely filled with concrete implementation solutions that also include 

- governing data – it’s about liberating it. Transforming data from a confusing, gated asset into an open, useful tool, accessible and actionable to everyone is realise on the top line (revenue) and bottom line (cost reduction and efficiency).
- implementing this incrementally 

Modern relational data platforms like Exasol, Snowflake, BigQuery, Redshift and Postgres are today able to handle transformation load on horizontal scale just like Hadoop - but on top of the relational model and using SQL for transformation. This is much easier than what Hadoop based platforms can offer - especially if you use Datavault automation and DBT in order to standardize your physical model. Together with column based DBMS, physical optimization like indexing, partitioning and manual DDL for the most part is not necessary anymore - making ELT extremely feasible and durable method.

Analytics Engineering together with DBT started in 2016 to enhance the user experience and add important functionality to the ELT approach:
- Collaboration on data teams
- Documentation with catalog functionality
- Software Engineering practices like versioning, ci pipeline and test driven
- Reducing complexity in ELT by removing the separation between DML and DDL

These simplifications make it easier then before to make the data and business analysts participate in the transformation work in ELT - that before was mostly in the hands of tech-savvy data engineers.

## Additional Ingestion Layer - Data warehouse

One of the big issues with data analytics efforts is and always has been the longevity, because after some time even with modularized models and potential re-use, with out a central layer of integration, re-use is not really sustainable.

Transparency - graphical modeling to map the requirements from the business process gives trust to the users of all sorts - not only analysts, but also developers.

That is the reason why we need to add Datavault into the pipeline and use DBT before - in the Hardrules - and after - in the Softrules - the Datavault integration, because of the Separation of Concerns:

- Hardrules implementation is source driven and should be an exception. Also Schema-On-Read views for PSA can be generated separately.
- Datavault should be business model first approach and have a clean map for the business requirements. Concerns for data integration and historization are isolated in the Datavault. Entity views delivery ELM like views that hide Datavault internal logic
- Softrules need to be close to the business user and less tech-savvy people - by using information hiding entity views in the Datavault, technical detail is away form the business logic.

Using our data platform tools and principles, it is shown how to utilize Continuous Integration to implement changes in this pipeline in isolation and incrementally, but with maintainability and transparency included. Through the use of disposable environments the execution of  the data pipeline is kept in synch with the SQL transformations developed in DBT and using only text based artifacts, that can be versioned and shared using GIT. On could call it DataOps as well.

## Artifacts
One reason for the success of Analytics Engineering is that despite simplifying transformation logic, it does implement good practices from software engineering in order to realize quality delivery and sharing between teams. Therefor users need to learn
- SQL
- GIT
- YAML
- using the command line

To utilize those engineering practices and sharing, all artifacts that we produce must be text based.
Through versioning we enable CI routines that are good practices in software engineering and also share transformation logic in SQL between teams.

In our projects we use the following file types:

SQL
- Model files contain SQL and jinja code
- Data tests contain SQL

YAML
- Model Schema files contain documentation and schema tests in YAML format
- DBT project files in YAML format
- DBTVault configuration files
- Workflow definition files for Argo in YAML format - similar to kubernetes manifest files. Due to our standard data architecture this workflows work based on ready templates
- Gitlab CI YAML for the CI pipeline definition
- Dockerfile definitions for project and documentation deployment
- Okteto YAML definition for remote connection into the development environments

JSON
- Datavault Builder specific json files in a standard folder structure for the deployment module

Others
- Datavault Builder specific rollout.sh shell scripts that are being generated
- Markdown and Asciidoc for documentation

### Monorepository
Since requirements mostly are vertical cuts through the data architecture, we use a mono repository that contains all parts of the data processing. Therefore we can follow one change request in only one merge/pull request in one repository, instead of going through multiple repositories.

```
├ dwh
├── .env.template
├── okteto.yml.template
├── readme.md
├── .gitignore
├── .stignore
├── .gitlab-ci.yml
└── 00_src
    └── fifa_hr
    └── src_002_hr
    └── src_003_hr
    └── psa
    └── Makefile
└── 01_dv
    └── dvb
    |   └── datavault_builderobjects / ...
    |   └── version.json
    |   └── rollout.sh
    └── dbt --> lineage project for dbt when using Datavault Builder
└── 02_app
    └── fifa_hr
    └── src_002_hr
    └── src_003_hr
    └── Makefile
└── 04_sbx
    └── sandbox001
    └── sandbox002
    └── sandbox003
└── doc
└── wf
    └── template
```

The distinction between DBT Hardrules and Softrules projects needs to be tagged in the dbt_project.yml:
```
name: 'fifa_hr'
version: '1.0.0'
config-version: 2
profile: 'dbt'

models:
  +post-hook: 'grant select on {{this}} to public'
  +tags:
    - 'hardrule' OR 'softrule'
```
The dwhgen generation tool and workflow logic is based on those tags.

## Implementation with DBT and Datavault
The separation between DBT and Datavault is relative simple separation of concerns:
1. All SQL transformation rule implementation go into DBT projects
These rules are separated into Hard- and Softrules - as per Datavault 2.0 definition. 
- The Hardrules being separated by source system - which should be the logical grouping that is also in Datavault being used as source_system metadata column. 
- The Softrules will be implemented after the Datavault for data delivery. Those are going to be grouped into DBT projects per application or report.

{{< figure src="/da_impl_detail.png" alt="Data Architecture Implementation View">}}

2. Datavault - as core or enterprise DWH layer using automation software
- Business Model driven CBC and NBR implementation in Datavault
- Integration of different source systems in Hubs and Links
- Historization of attribution in Satellites
- Providing entity views joining Hub, Links and Satellites together for information hiding
- Softrule driven model elements - so called Business Vault - persisting mostly Satellites derived from Softrules
- From our point of view we only model the Business Vault and Raw vault implementation should be the exception - in case the source system really cannot fit the target model. In these situations we would have Hub/Link/Satellite constructs, that do not conform to the business driven target model

## Makefiles for local development
For the Soft- and Hardrules dbt projects we suggest to use a Makefile for local development. Since argo workflow implementation is based on versioned artifacts (committed or container images), Makefiles are the easiest way to execute the multiple command on all or multiple DBT projects.
Here is an example Makefile:
```bash
do-everything:
	PRJ=./00_src/psa && dbt deps --project-dir $$PRJ \
		&& dbt seed --project-dir $$PRJ --profiles-dir $$PRJ

	PRJ=./00_src/fifa_hr && dbt deps --project-dir $$PRJ && \
		dbt run --target ci --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt test --target ci --project-dir $$PRJ --profiles-dir $$PRJ

	PRJ=./02_app/fifa && dbt deps --project-dir $$PRJ && \
		dbt seed --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt run --project-dir $$PRJ --profiles-dir $$PRJ
		dbt test --project-dir $$PRJ --profiles-dir $$PRJ
```
Then just calling make executes all DBT commands sequentially.
## Documenting continuously

### Combining documentation
Every single DBT repository is generating it's own documentation project. But to combine all documentation into one single project we are using the package feature of DBT. Therefor we add one aggregator project with all dbt projects imported as modules:
```
packages:
  - local: "{{ env_var('DBT_BASE_DIR') }}/00_src/fifa_hr"
  - local: "{{ env_var('DBT_BASE_DIR') }}/00_src/psa"
  - local: "{{ env_var('DBT_BASE_DIR') }}/02_app/fifa"
```

See also documentation deployment.
### Overview for analytical applications
Additionally we recommend to use a template to add as overview to the corresponding application projects. A useful template that describes the requirements for an analytics application from visuals down to the sources to be loaded is outlined here:

This template should be used as docs blocks in DBT:
```
{% docs __fifa__ %}

1. Report Requirements
    * Visualization or Prototype: Screenshot with comments
    * Questions: Open question list
2. Data Requirements (ELM)
    * Enterprise Data Story: short business description of what information is to be shown or delivered
    * ELM Model: Screenshot of the ELM to be implemented and the CBC list with CBCs and descriptions
    * Dimensional Model: Fact Table grain and their dimensions - Screenshot of PostIt model
    * links to the ELM artifacts from the workshop
3. Functional Dataflow
    * Interfaces: List of sources, the source interfaces and their technical keys (might be different from business key), that need to be imported and special hints how to access them
    * Golden Source Rules: In case of multiple sources being integrated we need
    * Hardrules: special Hardrules if necessary (Datatypes are being conformed automatically). Remember those should be exceptions and not change any datasets contents
    * Mapping and Softrules: any rules that need to be applied before delivering the mart schema or report

{% enddocs %}
```
Here we overwrite the overview for the fifa project/package. In case of main overview page you need to name the block __overview__.

### Attribution
To speed up development, make changes less expensive and simplify the automation of the Datavault, we recommend to document and change attribution of source systems only when concrete fields are being delivered in mart level projects.
These should definitely be curated by the business users during the ELM workshops and always being delivered selectively. Delivering attributes for future use with standard naming is fast in the beginning, but almost always fires back later.

Therefore all Q2 layer analytical application projects should have 
1. Concrete selection of attribution in fact and dimension models
2. All attributes names should conform to the naming conventions
3. All attributes need to have a business description
4. All attributes should be tested according to their expected values or value ranges

There are currently no reliable tools to test the coverage of tests and documentation on all platforms. Until those are available, we need to incorporate this in our reviews for the merge requests.
## Re-usage of mart models
In order to mimic a mart bus where dimensions or facts could be re-used in multiple projects, we recommend to create one Q2/02_app project with hose dimensions. Packages that need to use those can then import that common project as package import. Unfortunately, currently there is no easy mechanism to choose the models you like to have imported. This currently means that all artifacts are being either
- always shared completely - this might be overload for some uses of analytical applications that should have a small focus
- you need to re-name the models by having models select from the common imported once - in this case you need to come up with new unique names for those common dimensions in multiple application projects, if you need to combine their documentation into one HTML webapp.

You can however exclude models by maintaining lists of needed models in the dbt_project.yml:
```
models:
  common-dimension-project:
    +enabled: false
    dimension:
        dim_project:
            +enabled: true
```

## Test Driven Development approach

TDD is enforcing design by contract in software engineering projects. Although we are in the field of data analytics and not writing interfaces in code, we are interfacing with our data products in form of reports and SQL queries run against the Data Warehouse DBMS - which usually is a Star Schema.

The Star Schema is our contract to the outside world, that needs to be tested. Right from the start, we can use acceptance tests on this outcome in order to drive our User Story implementation.

The source datasets do travel through our standard pipeline from 

Q3-Source - Q3-PSA - Q1-Datavault - Q2-Star Schema

### DBT schema and data tests

As for DBT schema test - they are preferred, because integrated into the schema YAML, the big advantage is that they are visible in the catalog documentation.

Always include dbt_utils project in your packages, then you have the following schema tests available:
- [unique](https://docs.getdbt.com/reference/resource-properties/tests) - unique column
- [not_null](https://docs.getdbt.com/reference/resource-properties/tests) - not null coumn
- [accepted_values](https://docs.getdbt.com/reference/resource-properties/tests) - list of accepted values for a column
- [relationships](https://docs.getdbt.com/reference/resource-properties/tests) - testing relationship / foreign key
- [equal_rowcount](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#equal_rowcount-source) - test for same rowcount on two sets
- [fewer_rows_than](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#fewer-rows-then) - test this set to have less rows than compared set
- [equality](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#equality-source) - equal sets with alternative column list
- [expression_is_true](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#expression_is_true-source) - expression test on set in SQL syntax
- [recency](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#recency-source) - test interval on date column
- [at_least_one](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#at_least_one-source) - column has minimum one value
- [not_constant](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#not_constant-source) - column not constant in all rows
- [cardinality_equality](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#cardinality_equality-source) - column values cardinality compared to other's model column
- [unique_where](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#unique_where-source) - column unique with where clause
- [not_null_where](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#not_null_where-source) - not_null with where clause
- [not_null_proportion](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#not_null_proportion-source) - tests proportion of null values in a range 0-1.0
- [relationships_where](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#relationships_where-source) - same as relationships - but with predicate to filter rows
- [mutually_exclusive_ranges](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#mutually_exclusive_ranges-source) - tests overlap in values of two columns
- [unique_combination_of_columns](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#unique_combination_of_columns-source) - unique test for multiple columns
- [accepted_range](https://github.com/dbt-labs/dbt-utils/blob/0.8.0/README.md#accepted_range-source) - range values including filter

Using these schema tests we can also create acceptances tests on seed files:

```sql

```

### CI pipeline tests

The DBT seed feature is being used in the PSA DBT project - where those seeds are being stored as csv files under the data directory.
Below this directory organization of those seeds is freestyle - you can add CSV and their yml definitions in YAML files in any directory structure you like. Default is by source system - assuming we will not have hundreds of tables per source system.

{{< hint info >}}
**Seed feature in this form requires dbt version >= 0.21**  
In case you have dbt before this version, you need to have a flat hierarchy and define seed datatypes in dbt_project.yml. Please refer to the DBT documentation.
{{< /hint >}}

In order to combine results into CSV files over multiple relations, one could use - [Jailer](https://wisser.github.io/Jailer/) as tool. Here the user can define relations and their foreign keys in order to easily select fitting datasets from all relations combined.

The main purpose is to select minimal amount of datasets, that when you commit changes, the CI pipeline can send data through all parts of the data architecture. In this first meaning there is no real need to add special data, it could even be generated.

Secondly we need to make sure that certain expectations are being met. In this case we need to tweak those fields in order for those special tests to make sense - assure a column is really not null if we assume, or have certain value ranges.

Thirdly those same tests then could also easily be executed on production or integration, when loading real world data.

The decision wether to execute our pipeline against seed data or the PSA is encoded into the SOR - schema on read - views by using a special target-tag in the dbt-profiles YAML (see above flag --target ci ):
``` sql - Schema on Read with CI switch 
with

seed as (

{% if target.name == 'ci' %}
    select
        cast( textfield as varchar(2000000)) as "TEXTFIELD",
        cast( timestampfield as timestamp) as "TIMESTAMPFIELD",
        cast( integerfield as bigint) as "INTEGERFIELD",
        cast( "DATEFIELD" as date) as "DATEFIELD",
        cast( decimalfield as decimal(36,12)) as "DECIMALFIELD",

    from {{ source('sourcename','sourceinterface') }} 

{%- else %}
    select 'seed' as seedname from dual
{% endif %}

),

sor_view_delta as (

    select 
        cast( json_value( payload, '$.textfield') as varchar(2000000)) as "TEXTFIELD",
        cast( json_value( payload, '$.timestampfield' as timestamp)  as "TIMESTAMPFIELD",
        cast( json_value( payload, '$.integerfield') as bigint) as "INTEGERFIELD",
        cast( "DATEFIELD" as date) as "DATEFIELD",
        cast( "DECIMALFIELD" as decimal(36,12)) as "DECIMALFIELD",
    from (
     select payload from (
    		select 
                payload, 
                row_number() over(partition by 
                    source_name, 
                    interface_schema, 
                    interface_name, 
                    bk order by ldts desc) as latestrecord

    		from yyy_q3_psa.psa_insert
		    where 
                source_name = 'sourcename' and 
                interface_schema='sourceschema' and 
                interface_name = 'sourceinterface'

    	) where latestrecord=1
    )
)
{% if target.name == 'ci' %}
select * from seed
{% else %}
select * from sor_view_delta
{% endif %}
```

### Q3 - Source based tests - Ingest + PSA

From experience our expectations on data quality from sources systems is very low. Therefor testing in PSA or Hardrule layer does not make much sense.

There are two expectations that are still important for integration into the Q1 facts layer:
- technical or business key `unique` and `not_null`schema tests should be present
- structuring source data for easy matching to the Datavault target model (if possible) - that is what Hardrules are for - but we need to make sure we do not change any source data. This can be tested with Tests from the Datavault entity views against the PSA.

{{< hint info >}}
**Data Profiling and Measuring Data Quality**  
In contrast to test efforts on data we want to integrate, the PSA is a perfect place to measure source data quality. But please always have this executed in distinct and separate pipelines from your data integration or analytics efforts.
{{< /hint >}}

### Q1 - Datavault testing - DON'T test
Your Datawarehouse automation does not need any testing. If you do not trust your Warehouse automation tool to properly integrate Hub, Link and Satellite as to Datavault Load patterns, then use a different one.
In contrast - this is source data, that can be proven to be 1:1 according to Entity View tests
### Q2 - Regression and Acceptance testing
- Entity Views against PSA / Hardrule

For the acceptance tests that are defined in our analytical applications Softrules, we also can use CSV as seed datasets and then use those data sets to define source data (delivered by the Datavault) and the expected outcome.
To keep things simple and fast those datasets should also be as small as possible. 
The data test would then always look quite similar - by just using SQL SET features and also including a count comparison:
``` sql
(
    select * from {{ ref('source_model') }}
    except
    select * from {{ ref('expected_target') }}
)

union all

(
    select * from {{ ref('expected_target') }}
    except
    select * from {{ ref('source_model') }}
)
```

``` sql
with

count_diff (n) as (
    select
        (select count(*) from {{ ref('source_model') }})
        -
        (select count(*) from {{ ref('expected_target') }})
    
    from dual
)

select n

from count_diff

where n != 0
```

Special care needs to be taken for the source datasets in the PSA, that have to fit the expected seed in the acceptance test. In case multiple tests need different sets from the same seed files - recommendation is to add ```testname``` column to the seed file. A simple where clause selects only the dataset for the corresponding test that executes.

## Operations in modern containers - Kubernetes
Topping of our modern data platform, we are using the state of the art container execution cluster and orchestrator - Kubernetes.
This gives us several advantages:
- Re-using existing tools for observability and alerting (Prometheus, AlertManager, Grafana, Promscale and TimescaleDB)
- The kubernetes-native argo workflow engine for cluster-wide execution of the data pipeline - we can use this in the same way for Continuous Integration and for productive scheduling.
- Namespaces for ready network isolation of our Statefulsets (database) and Pods (Datavault Builder, DBT, Argo, Okteto)

Also, from existing DevOps experiences we can re-use development utilities like k9s and okteto in order to ease local development.

These tools require a little bit of understanding of containers and kubernetes from the users, but through the scripts (envinit, envlist, envcreate, envdelete) and above mentioned tools, mostly it feels like "normal" DBT - regardless where my disposable env is running the server components. Therefor we are convinced that these will be outweighed by the advantages from the DataOps like

- higher quality development artifacts
- more transparency for the end users through business model first approach
- longevity due to clean Datavault model 

And all those will ultimately contribute to more trust from our end users, that need those analytical applications for their business to thrive.


# Further Reading
- [What is Analytics Engineering](https://www.getdbt.com/what-is-analytics-engineering/)
- [Trunk Based Development](https://trunkbaseddevelopment.com/)
- [Monorepo](https://en.wikipedia.org/wiki/Monorepo)
- [Monorepo Discussion](https://www.youtube.com/watch?v=VvcJGjjEyKo)
- [TDD discussion](https://www.youtube.com/watch?v=llaUBH5oayw)
- [TDD](https://en.wikipedia.org/wiki/Test-driven_development)
- [Test Data Tool](https://wisser.github.io/Jailer/)
